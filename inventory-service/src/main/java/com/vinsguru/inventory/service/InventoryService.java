package com.vinsguru.inventory.service;

import com.vinsguru.dto.InventoryRequestDTO;
import com.vinsguru.dto.InventoryResponseDTO;
import com.vinsguru.enums.InventoryStatus;
import org.springframework.stereotype.Service;

import java.util.HashMap;
import java.util.Map;

@Service
public class InventoryService {

    private final Map<Integer, Integer> productInventory = new HashMap<>(Map.of(1, 5, 2, 5, 3, 5));

    public InventoryResponseDTO deductInventory(final InventoryRequestDTO requestDTO) {
        int quantity = this.productInventory.getOrDefault(requestDTO.getProductId(), 0);
        InventoryResponseDTO responseDTO = new InventoryResponseDTO();
        responseDTO.setOrderId(requestDTO.getOrderId());
        responseDTO.setUserId(requestDTO.getUserId());
        responseDTO.setProductId(requestDTO.getProductId());
        responseDTO.setStatus(InventoryStatus.UNAVAILABLE);
        if (quantity > 0) {
            responseDTO.setStatus(InventoryStatus.AVAILABLE);
            this.productInventory.put(requestDTO.getProductId(), quantity - 1);
        }
        return responseDTO;
    }

    public void addInventory(final InventoryRequestDTO requestDTO) {
        this.productInventory
                .computeIfPresent(requestDTO.getProductId(), (k, v) -> v + 1);
    }

}
