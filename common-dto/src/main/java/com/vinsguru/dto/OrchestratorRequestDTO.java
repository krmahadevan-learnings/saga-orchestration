package com.vinsguru.dto;

import lombok.Data;

import java.util.UUID;

@Data
public class OrchestratorRequestDTO {

    private Integer userId = 0;
    private Integer productId = 0;
    private UUID orderId;
    private Double amount = 0.0;

}
