package com.vinsguru.payment.service;

import com.vinsguru.dto.PaymentRequestDTO;
import com.vinsguru.dto.PaymentResponseDTO;
import com.vinsguru.enums.PaymentStatus;
import org.springframework.stereotype.Service;

import java.util.HashMap;
import java.util.Map;

@Service
public class PaymentService {

    private final Map<Integer, Double> userBalance = new HashMap<>(
            Map.of(1, 1000d, 2, 1000d, 3, 1000d)
    );

    public PaymentResponseDTO debit(final PaymentRequestDTO requestDTO) {
        double balance = userBalance.getOrDefault(requestDTO.getUserId(), 0d);
        PaymentResponseDTO dto = new PaymentResponseDTO();
        dto.setAmount(requestDTO.getAmount());
        dto.setUserId(requestDTO.getUserId());
        dto.setOrderId(requestDTO.getOrderId());
        dto.setStatus(PaymentStatus.PAYMENT_REJECTED);
        if (balance >= requestDTO.getAmount()) {
            dto.setStatus(PaymentStatus.PAYMENT_APPROVED);
            this.userBalance.put(requestDTO.getUserId(), balance - requestDTO.getAmount());
        }
        return dto;
    }

    public void credit(PaymentRequestDTO dto) {
        this.userBalance.computeIfPresent(dto.getUserId(), (k, v) -> v + dto.getAmount());
    }

}
