package com.vinsguru.saga.service;

import java.util.List;

public class OrderWorkflow implements Workflow {
    private final WorkflowStep[] steps;

    public OrderWorkflow(WorkflowStep...steps) {
        this.steps = steps;
    }

    @Override
    public List<WorkflowStep> getSteps() {
        return List.of(steps);
    }

}
