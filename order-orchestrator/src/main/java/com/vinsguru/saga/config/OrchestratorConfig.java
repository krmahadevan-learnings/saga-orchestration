package com.vinsguru.saga.config;

import com.vinsguru.dto.OrchestratorRequestDTO;
import com.vinsguru.dto.OrchestratorResponseDTO;
import com.vinsguru.saga.service.OrchestratorService;
import lombok.AllArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import reactor.core.publisher.Flux;

import java.util.function.Function;

@Configuration(proxyBeanMethods = false)
@AllArgsConstructor
@Slf4j
public class OrchestratorConfig {

    private final OrchestratorService orchestratorService;

    @Bean
    public Function<Flux<OrchestratorRequestDTO>, Flux<OrchestratorResponseDTO>> processor() {
        return flux -> flux
                .log()
                .flatMap(this.orchestratorService::orderProduct)
                .doOnNext(dto -> log.info("Status : {}", dto.getStatus()));
    }
}
