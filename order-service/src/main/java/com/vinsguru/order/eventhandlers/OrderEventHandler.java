package com.vinsguru.order.eventhandlers;

import com.vinsguru.dto.OrchestratorRequestDTO;
import com.vinsguru.dto.OrchestratorResponseDTO;
import com.vinsguru.order.service.OrderEventUpdateService;
import lombok.AllArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import reactor.core.publisher.DirectProcessor;
import reactor.core.publisher.Flux;
import reactor.core.publisher.Sinks;

import java.util.function.Consumer;
import java.util.function.Supplier;

@Configuration(proxyBeanMethods = false)
@AllArgsConstructor
@Slf4j
public class OrderEventHandler {

    private final OrderEventUpdateService service;

    @Bean
    public Supplier<Flux<OrchestratorRequestDTO>> supplier(Flux<OrchestratorRequestDTO> flux) {
        return () -> flux;
    }

    @Bean
    public Sinks.Many<OrchestratorRequestDTO> sink() {
        return Sinks.many().unicast().onBackpressureBuffer();
    }

    @Bean
    public Flux<OrchestratorRequestDTO> flux(Sinks.Many<OrchestratorRequestDTO> sink) {
        return sink.asFlux();
    }

    @Bean
    public Consumer<Flux<OrchestratorResponseDTO>> consumer() {
        return flux -> flux
                .doOnNext(dto -> log.info("Consuming :: {}", dto))
                .flatMap(this.service::updateOrder)
                .subscribe();
    }
}
