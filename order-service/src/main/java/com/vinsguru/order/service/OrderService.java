package com.vinsguru.order.service;

import com.vinsguru.dto.OrchestratorRequestDTO;
import com.vinsguru.dto.OrderRequestDTO;
import com.vinsguru.dto.OrderResponseDTO;
import com.vinsguru.enums.OrderStatus;
import com.vinsguru.order.entity.PurchaseOrder;
import com.vinsguru.order.repository.PurchaseOrderRepository;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;
import reactor.core.publisher.Flux;
import reactor.core.publisher.Mono;
import reactor.core.publisher.Sinks;

import java.util.Map;

@Service
@RequiredArgsConstructor
public class OrderService {

    // product price map
    private static final Map<Integer, Double> PRODUCT_PRICE = Map.of(
            1, 100d,
            2, 200d,
            3, 300d
    );

    private final PurchaseOrderRepository purchaseOrderRepository;
    private final Sinks.Many<OrchestratorRequestDTO> sink;

    public Mono<PurchaseOrder> createOrder(OrderRequestDTO orderRequestDTO) {
        return this.purchaseOrderRepository.save(dtoToEntity(orderRequestDTO))
                .doOnNext(e -> orderRequestDTO.setOrderId(e.getId()))
                .doOnNext(e -> this.emitEvent(orderRequestDTO));
    }

    public Flux<OrderResponseDTO> getAll() {
        return this.purchaseOrderRepository.findAll().map(this::entityToDto);
    }

    private void emitEvent(OrderRequestDTO orderRequestDTO) {
        this.sink.tryEmitNext(getOrchestratorRequestDTO(orderRequestDTO));
    }

    private static PurchaseOrder dtoToEntity(OrderRequestDTO dto) {
        PurchaseOrder purchaseOrder = new PurchaseOrder();
        //Don't use the order id that was sent to us. It is a PK, so we need to have the DB take care of
        // generating it.
//        purchaseOrder.setId(dto.getOrderId());
        purchaseOrder.setProductId(dto.getProductId());
        purchaseOrder.setUserId(dto.getUserId());
        purchaseOrder.setStatus(OrderStatus.ORDER_CREATED);
        purchaseOrder.setPrice(PRODUCT_PRICE.get(purchaseOrder.getProductId()));
        return purchaseOrder;
    }

    private OrderResponseDTO entityToDto(PurchaseOrder purchaseOrder) {
        OrderResponseDTO dto = new OrderResponseDTO();
        dto.setOrderId(purchaseOrder.getId());
        dto.setProductId(purchaseOrder.getProductId());
        dto.setUserId(purchaseOrder.getUserId());
        dto.setStatus(purchaseOrder.getStatus());
        dto.setAmount(purchaseOrder.getPrice());
        return dto;
    }

    private static OrchestratorRequestDTO getOrchestratorRequestDTO(OrderRequestDTO orderRequestDTO) {
        OrchestratorRequestDTO dto = new OrchestratorRequestDTO();
        dto.setUserId(orderRequestDTO.getUserId());
        dto.setAmount(PRODUCT_PRICE.get(orderRequestDTO.getProductId()));
        dto.setOrderId(orderRequestDTO.getOrderId());
        dto.setProductId(orderRequestDTO.getProductId());
        return dto;
    }

}
