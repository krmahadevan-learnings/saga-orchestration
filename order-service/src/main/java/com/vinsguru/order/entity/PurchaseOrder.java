package com.vinsguru.order.entity;

import com.vinsguru.enums.OrderStatus;
import lombok.Data;
import lombok.ToString;
import org.springframework.data.annotation.Id;
import java.util.UUID;

@Data
@ToString
public class PurchaseOrder {

    @Id
    private UUID id;
    private Integer userId = 0;
    private Integer productId = 0;
    private Double price = 0.0;
    private OrderStatus status;

}